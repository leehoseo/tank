#pragma strict

var snd : AudioClip;                 

function OnTriggerEnter(coll : Collider) {  
    AudioSource.PlayClipAtPoint(snd, transform.position);
    
    Destroy(gameObject);
    
    if (coll.gameObject.tag == "WALL") 
    {                                                     
        Destroy(coll.gameObject);
   	}
   	
   	if (coll.gameObject.tag == "TANK") 
    {               
    	Score.lose++; 
    	if(Score.lose >= 3)
    	{
    		Application.LoadLevel("LoseGame");
    	}             
   	}
   	
   	if (coll.gameObject.tag == "ENEMY") 
    {
    	Score.hit++; 
    	if(Score.hit >= 3)
    	{
    		Application.LoadLevel("WinGame");
    	}
   	}
} 