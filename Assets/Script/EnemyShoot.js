#pragma strict

var power = 1000000;                
var bullet : Transform;  
var explosion: Transform;  
var snd : AudioClip;  


 
private var ftime : float = 0.0; 

function Update () 
{
 	ftime += Time.deltaTime;     
 
	var hit : RaycastHit;  
	var spPoint = GameObject.Find("SpawnPoint2");
	
	var fwd = transform.TransformDirection(Vector3.forward);
	
    if (Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false)
    {
    	return;      
	}
	
	if (ftime < 2)
	{
		return; 
	}
	 
    Instantiate(explosion, spPoint.transform.position, Quaternion.identity);             
    var obj = Instantiate(bullet, spPoint.transform.position, Quaternion.identity);     
    obj.rigidbody.AddForce(fwd * power);
    AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);     
    ftime = 0;                
}