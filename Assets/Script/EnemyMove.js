#pragma strict
 
 var target : Transform; 
 var rotAng = 15;

function Update () 
{
	transform.LookAt(target);
    var amtToRot = rotAng * Time.deltaTime;
    transform.Translate(Vector3.forward * 0.1); 
    transform.RotateAround(Vector3.zero, Vector3.up, amtToRot);
}