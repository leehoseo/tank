#pragma strict

var power = 1000000;                
var bullet : Transform;  
var explosion: Transform;  
var snd : AudioClip;      

function Update () 
{
 	if (Input.GetButtonDown("Fire1")) 
 	{
        var spPoint = GameObject.Find("SpawnPoint");               
        var myBullet = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation);
        myBullet.rigidbody.AddForce(spPoint.transform.forward * power);
        Instantiate(explosion, spPoint.transform.position, Quaternion.identity);    
     	AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);      
 	}
}

